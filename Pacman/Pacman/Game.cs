using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Pacman
{
    /// <summary>
    /// Pacman written in C# and XNA by Matthias Lexer
    /// 
    /// -------------------------------Hotkeys:-----------------------------------
    /// 
    /// F1 - Next Picture for Pacman (another color)
    /// F2 - Last Picture for Pacman (-------"-----)
    /// F11 - EXIT
    /// 
    /// if you won or lose the game you can press "Enter" to play a New Game
    /// </summary>
    /// 
    public class Game : Microsoft.Xna.Framework.Game
    {
        #region Variablendeklarierung
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;


        //Klassen
        ClassField CField;
        ClassConstants CConstants;

        //Texturen
        Texture2D texBody;
        Texture2D texGhost;
        Texture2D texPacMan;
        Texture2D texPoint;

        //F�r Men�
        SpriteFont menuFont;
        SpriteFont stateFont;
        int iGameOrMenu;                //  1-Men�  2-Game 

        bool bpaused;
        bool bwon;
        bool blose;

        //Integer Variablen
        public int iActualLevel;

        //F�r Punkte + Gewonnen etc. 
        public int iPoints;             //Punkte
        string strLoseMessage;          //zeigt sich wenn man verloren hat 
        SpriteFont pointsFont;          //Schrift der Punkte
        String strPoints;               //Punkte in String

        int iPacmanX;
        int iPacmanY;
        int iState;

        int iPictureNumber;
        int iPictureNumberMultiplied;

        //Geister

        int iGhostX1;
        int iGhostY1;
        int iGhostX2;
        int iGhostY2;
        int iGhostX3;
        int iGhostY3;
        int iGhostX4;
        int iGhostY4;

        //--------------------------F�r Move - Methode---------------------//
        int iXNew;
        int iYNew;
        int iDirPacman;

        int iDirGhost;
        int iDirGhost2;
        int iDirGhost3;
        int iDirGhost4;

        int iDir;
        Random Rnd = new Random();
        #endregion

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.IsFullScreen = false;

            //Normale Variablenzuweisungen
            iActualLevel = 1;
            CConstants = new ClassConstants();
            //Men�
            iGameOrMenu = 1;

            //H�he - Breite (Aufl�sung)
            graphics.PreferredBackBufferHeight = CConstants.iScreenHeight;
            graphics.PreferredBackBufferWidth = CConstants.iScreenWidth;

            //Fenstertitel
            Window.Title = "PacmanML";

            //Einstellbar bei gutem Optionsmen�:

                //Einstellen welches Bild - 0 - 7
                iPictureNumber = 0;
        }
        protected override void Initialize()
        {
            Init();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);


            //Texturen
            texBody = Content.Load<Texture2D>("block");
            texGhost = Content.Load<Texture2D>("ghost");
            texPacMan = Content.Load<Texture2D>("pacman");
            texPoint = Content.Load<Texture2D>("point");

            //Fonts
            menuFont = Content.Load<SpriteFont>("menuFont");
            pointsFont = Content.Load<SpriteFont>("points");
            stateFont = Content.Load<SpriteFont>("stateFont");
        }
        protected override void Update(GameTime gameTime)
        {
            KeyboardState currentState = Keyboard.GetState();
            Keys[] currentKeys = currentState.GetPressedKeys();

            if (!bpaused)
            {
                if (iPoints == CField.iPointNumber)
                {
                    
                    bwon = true;
                    StopGame();
                }

                #region Hotkeys
                //-------------------------------------------------------HOTKEYS-------------------------------------------------------//
                

                foreach (Keys keys in currentKeys)
                {
                    if (keys == Keys.F11) Exit();

                    if (gameTime.TotalGameTime.Milliseconds % 30 == 0)
                    {
                        if (keys == Keys.F1)
                        {
                            if (iPictureNumber != 7) iPictureNumber++;
                            else iPictureNumber = 1;
                        }
                        if (keys == Keys.F2)
                        {
                            if (iPictureNumber != 0) iPictureNumber--;
                            else iPictureNumber = 7;
                        }
                    }
                }

                #endregion
                #region Pacman
                if (iGameOrMenu == 2)
                {
                    //----------------------------------------------Pacman - Bewegung--------------------------------------------------//

                    /// <summary>
                    ///Erzeugt Zufallszahl zwischen und 3 --> 4 Zahlen --> 4 Richtungen
                    ///0 = nach oben
                    ///1 = nach unten
                    ///2 = nach rechts
                    ///3 = nach links
                    /// </summary>
                    /// 

                   
                    if (gameTime.TotalGameTime.Milliseconds % 10 == 0)
                    { 
                        // �ndert die Richtung des Pacmans
                        foreach (Keys keys in currentKeys)
                        {
                            if (keys == Keys.Up) iDirPacman = 0;
                            if (keys == Keys.Down) iDirPacman = 1;
                            if (keys == Keys.Left) iDirPacman = 3;
                            if (keys == Keys.Right) iDirPacman = 2;
                        }
                    }
                    if (gameTime.TotalGameTime.Milliseconds % 20 == 0)
                    {   
                        //l�sst den Pacman dann laufen
                        {
                            iState = 1;
                            Move(iState, iPacmanX, iPacmanY, iDirPacman);

                        }
                    }
                }
                #endregion
                #region Ghosts

                if (iGameOrMenu == 2)
                {
                    iState = 2;     //Geist 1
                    if (gameTime.TotalGameTime.Milliseconds % 20 == 0)
                    {
                        Move(iState, iGhostX1, iGhostY1, iDirGhost);
                    }

                    iState = 3;     //Geist 2
                    if (gameTime.TotalGameTime.Milliseconds % 20 == 0)
                    {
                        Move(iState, iGhostX2, iGhostY2, iDirGhost);
                    }

                    iState = 4;     //Geist 3
                    if (gameTime.TotalGameTime.Milliseconds % 20 == 0)
                    {
                        Move(iState, iGhostX3, iGhostY3, iDirGhost);
                    }

                    iState = 5;     //Geist 4
                    if (gameTime.TotalGameTime.Milliseconds % 20 == 0)
                    {
                        Move(iState, iGhostX4, iGhostY4, iDirGhost);
                    }
                }
                #endregion
            }

            #region Menu
            if (gameTime.TotalGameTime.Milliseconds % 5 == 0)
                {
                    foreach (Keys keys in currentKeys)
                    {
                    if (iGameOrMenu == 1) //also falls das Men� aktiv ist
                    {
                    
                            if (keys == Keys.Enter)
                            { 
                                iGameOrMenu = 2; NewGame(); 
                            }

                            if (keys == Keys.F11) this.Exit();
                        }
                
                    if (iGameOrMenu == 2) // also falls spiel aktiv
                    {
                        if (keys == Keys.F5)
                        { 
                            StopGame();
                            bpaused = true;
                        }
                        if (keys == Keys.Escape) 
                        {
                            StopGame();
                            iGameOrMenu = 1;
                        }
                    }
                }
            }
            #endregion

            if (blose || blose)
            {
                foreach (Keys keys in currentKeys)
                {
                    if (keys == Keys.Enter)
                    {
                        NewGame();
                    }

                    if (keys == Keys.F11) Exit();
                }
            }
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            //Hintergrund - Farbe
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            #region ZeichnenVonGeistPacmanBlocksPunkten
            if (iGameOrMenu == 2)
            {
                //---------------------------------------------------------------- Alles Zeichnen--------------------------------------------------------------------//
                int iX;
                int iY;

                for (iY = 0; iY < CField.oTypeToDraw.GetLength(1); iY++)
                {
                    for (iX = 0; iX < CField.oTypeToDraw.GetLength(0); iX++)
                    {
                        switch (CField.oTypeToDraw[iX, iY])
                        {
                            case enFieldType.Body:
                                spriteBatch.Draw(texBody, new Vector2(iX * CConstants.iBlockSize, iY * CConstants.iBlockSize), Color.White);

                                break;

                            case enFieldType.Point:
                                spriteBatch.Draw(texPoint, new Rectangle(iX * CConstants.iBlockSize + 5, iY * CConstants.iBlockSize + 5, 10, 10), Color.White);

                                break;


                            case enFieldType.Pacman:

                                //Rotation
                                int iFrameTime;
                                iFrameTime = gameTime.TotalGameTime.Milliseconds / (1000 / 14);

                                iPictureNumberMultiplied = iPictureNumber * 128;

                                if(iFrameTime > 7) iFrameTime = 14 - iFrameTime;
                                spriteBatch.Draw(texPacMan, new Rectangle(iX * CConstants.iBlockSize, iY * CConstants.iBlockSize, CConstants.iBlockSize, CConstants.iBlockSize), new Rectangle(iFrameTime * 128, iPictureNumberMultiplied , 128, 128), Color.White, 0, Vector2.Zero, SpriteEffects.None, 0f);
                                //Rotataion Ende


                                break;

                            case enFieldType.Ghost1:
                                spriteBatch.Draw(texGhost, new Vector2(iX * CConstants.iBlockSize, iY * CConstants.iBlockSize), Color.White);

                                break;

                            case enFieldType.Ghost2:
                                spriteBatch.Draw(texGhost, new Vector2(iX * CConstants.iBlockSize, iY * CConstants.iBlockSize), Color.White);
                                break;

                            case enFieldType.Ghost3:
                                spriteBatch.Draw(texGhost, new Vector2(iX * CConstants.iBlockSize, iY * CConstants.iBlockSize), Color.White);
                                break;

                            case enFieldType.Ghost4:
                                spriteBatch.Draw(texGhost, new Vector2(iX * CConstants.iBlockSize, iY * CConstants.iBlockSize), Color.White);

                                break;
                        }
                    }
                }
            }

            #endregion

            #region ZeichnenDesStringsLinksOben

            if (iGameOrMenu == 2)
            {
                if (bwon)
                {
                    strLoseMessage = "You Won           -       Press Enter to play a New Game";
                    spriteBatch.DrawString(pointsFont, strLoseMessage, new Vector2(3, 3), Color.White);
                }
                else
                {
                    if (blose)
                    {
                        strLoseMessage = "You Lose          -       Press Enter to play a New Game";
                        spriteBatch.DrawString(pointsFont, strLoseMessage, new Vector2(3, 3), Color.White);
                    }

                    else
                    {
                        strPoints = "Points : " + iPoints.ToString();
                        spriteBatch.DrawString(pointsFont, strPoints, new Vector2(3, 3), Color.White);
                    }
                }

            }
            #endregion

            #region Menu
            if (iGameOrMenu == 1)
            {
                spriteBatch.DrawString(menuFont, "New Game", new Vector2(((CConstants.iScreenWidth / 2) - 90), 180), Color.Green);

                spriteBatch.DrawString(stateFont, "Hotkeys: ", new Vector2(((CConstants.iScreenWidth / 2) - 110), CConstants.iScreenHeight - 125), Color.Green);
                spriteBatch.DrawString(stateFont, "Enter - Start Game", new Vector2(((CConstants.iScreenWidth / 2) - 110), CConstants.iScreenHeight - 105), Color.White);
                spriteBatch.DrawString(stateFont, "F1 - Next Picture for Pacman", new Vector2(((CConstants.iScreenWidth / 2) - 110), CConstants.iScreenHeight - 95), Color.White);
                spriteBatch.DrawString(stateFont, "F2 - Last Picture for Pacman", new Vector2(((CConstants.iScreenWidth / 2) - 110), CConstants.iScreenHeight - 85), Color.White);
                spriteBatch.DrawString(stateFont, "F11 - EXIT", new Vector2(((CConstants.iScreenWidth / 2) - 110), CConstants.iScreenHeight - 75), Color.White);
                spriteBatch.DrawString(stateFont, "v.1.0 (BETA) - by Matthias Lexer", new Vector2(((CConstants.iScreenWidth / 2) - 110), CConstants.iScreenHeight-45), Color.White);
            }
            #endregion
            spriteBatch.End();
            base.Draw(gameTime);
        }
        public void Move(int iState, int iX, int iY, int iDirPacman)
        {

            /// <summary>
            ///Erzeugt Zufallszahl zwischen und 3 --> 4 Zahlen --> 4 Richtungen
            ///0 = nach oben
            ///1 = nach unten
            ///2 = nach rechts
            ///3 = nach links
            /// </summary>

            #region ZuweisungVonXundYundDirection
            if (iState == 1)        //pacmanspezifisch
            {
                iDir = iDirPacman;
            }

            if (iState == 2)        //Geist 1
            {
                iDir = iDirGhost;
                if (iDir == 0) //iDir = Rnd.Next(1, 4);
                iDirGhost = iDir;
                if (iX == 0) iX = (int)CField.oGhostSpawnPoint.X;
                if (iY == 0) iY = (int)CField.oGhostSpawnPoint.Y;
            }

            if (iState == 3)        //Geist 2
            {
                iDir = iDirGhost2;
                if (iDir == 0) //iDir = Rnd.Next(1, 4);
                iDirGhost2 = iDir;
                if (iX == 0) iX = (int)CField.oGhost2SpawnPoint.X;
                if (iY == 0) iY = (int)CField.oGhost2SpawnPoint.Y;
            }

            if (iState == 4)        //Geist 3
            {
                iDir = iDirGhost3;
                if (iDir == 0) //iDir = Rnd.Next(1, 4);
                iDirGhost3 = iDir;
                if (iX == 0) iX = (int)CField.oGhost3SpawnPoint.X;
                if (iY == 0) iY = (int)CField.oGhost3SpawnPoint.Y;
            }

            if (iState == 5)        //Geist 4
            {
                iDir = iDirGhost4;
                if (iDir == 0) //iDir = Rnd.Next(1, 4);
                iDirGhost4 = iDir;
                if (iX == 0) iX = (int)CField.oGhost4SpawnPoint.X;
                if (iY == 0) iY = (int)CField.oGhost4SpawnPoint.Y;
            }
            #endregion

            if (iDir == 0) { iXNew = iX; iYNew = iY - 1; }
            if (iDir == 1) { iXNew = iX; iYNew = iY + 1; }
            if (iDir == 2) { iXNew = iX + 1; iYNew = iY; }
            if (iDir == 3) { iXNew = iX - 1; iYNew = iY; }

            #region Randprobleme
            if (iXNew < 0)
            {
                iXNew = 0;
                if (iState > 1) iDir = Rnd.Next(1, 4);
            }

            if (iXNew >= CConstants.iScreenWidth / 20)
            {
                iXNew = CConstants.iScreenWidth / 20 - 1;
                if (iState > 1) iDir = Rnd.Next(1, 4);
            }

            if (iYNew < 0)
            {
                iYNew = 0;
                if (iState > 1) iDir = Rnd.Next(1, 4);
            }

            if (iYNew >= CConstants.iScreenHeight / 20)
            {
                iYNew = CConstants.iScreenHeight / 20 - 1;
                if (iState > 1) iDir = Rnd.Next(1, 4);
            }
            #endregion

            #region Pr�fenWasAufDemN�chstenFeldIstDannBesetzen

            #region WennLeer
            //Wenn Leer dann Besetzen
            if (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Empty)
            {
                CField.oTypeToDraw[iX, iY] = enFieldType.Empty;

                if (iState == 1) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Pacman;
                if (iState == 2) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost1;
                if (iState == 3) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost2;
                if (iState == 4) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost3;
                if (iState == 5) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost4;
            }


            else
            {
            #endregion
                #region WennPunkt
                //Wenn Punkt dann Pacman drauf --> Punkte++ , Geist dr�berlaufen auf eigentlicher Geistposition Punkt erstellen
                if (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Point)
                {
                    if (iState == 1)        //Wenn Pacman
                    {
                        CField.oTypeToDraw[iX, iY] = enFieldType.Empty;
                        CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Pacman;
                        iPoints++;
                    }

                    if (iState > 1)        //Wenn Geist
                    {
                        if (iState == 2) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost1;
                        if (iState == 3) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost2;
                        if (iState == 4) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost3;
                        if (iState == 5) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost4;


                        CField.oTypeToDraw[iX, iY] = enFieldType.Point;
                    }

                }
                else
                {
                #endregion
                    #region PacmanGhostKoolision(OrGhostGhostKoolision)
                    //Checken ob Pacman gegen Geist rennt
                    if (iState == 1)
                    {
                        if ((CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost1) || (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost2) || (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost3) || (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost4))
                        {
                            //strLoseMessage = "You Lose ! Your Score was: " + iPoints.ToString();
                            iX = iXNew;
                            iY = iYNew;

                            blose = true;
                            StopGame();
                        }
                    
                    }
                    else
                    {
                        //Checken ob Geist gegen Pacman rennt
                        if (iState > 1)
                        {
                            if (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Pacman)
                            {
                                //strPoints = "You Lose ! Your Score was: " + iPoints.ToString();
                                StopGame();
                                blose = true;
                            }

                            if ((CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost1) || (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost2) || (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost3) || (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Ghost4))
                            {
                                iXNew = iX;
                                iYNew = iY;
                                iDir = Rnd.Next(1, 4);
                            }
                        }
                        
                            if (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Body)
                            {
                                iXNew = iX;
                                iYNew = iY;
                                if (iState > 1) iDir = Rnd.Next(1, 4);
                            }
                        }
                    

                    

                    #endregion


            #region Zuweisung
            if (iState == 1)
            { iXNew = iX; iYNew = iY; iDirPacman = iDir; }
            if (iState == 2)
            { iDir = Rnd.Next(4); iDirGhost = iDir; }
            if (iState == 3)
            { iDir = Rnd.Next(4); iDirGhost2 = iDir; }
            if (iState == 4)
            { iDir = Rnd.Next(4); iDirGhost3 = iDir; }
            if (iState == 5)
            { iDir = Rnd.Next(4); iDirGhost4 = iDir; }

                }
            }
                    #endregion

            #endregion

            #region KommentierterText
            /*
            int iVersuche;


            for (iVersuche = 0; iVersuche < 5; iVersuche++)
            {
                switch (iDir)
                {
                    case 0: iXNew = iX; iYNew = iY + 1; break;      //Nach Oben
                    case 1: iXNew = iX; iYNew = iY - 1; break;      //Nach Unten
                    case 2: iXNew = iX + 1; iYNew = iY; break;      //Nach Rechts
                    case 3: iXNew = iX - 1; iYNew = iY; break;      //Nach Links

                    default: iXNew = iX; iYNew = iY; break;
                }
                #region Rand�berpr�fung
                //-------------------------------------Rand �berpr�fen-------------------------------//
                if (iXNew < 0)
                {
                    iXNew = 0;
                    if (iState == 2) iDir = Rnd.Next(4);
                }

                if (iYNew < 0)
                {
                    iYNew = 0;
                    if (iState == 2) iDir = Rnd.Next(4);
                }

                if (iXNew > CConstants.iScreenWidth / CConstants.iBlockSize - 1)
                {
                    iXNew = CConstants.iScreenWidth / CConstants.iBlockSize - 1;
                    if (iState == 2) iDir = Rnd.Next(4);
                }

                if (iYNew > CConstants.iScreenHeight / CConstants.iBlockSize - 1)
                {
                    iYNew = CConstants.iScreenHeight / CConstants.iBlockSize - 1;
                    if (iState == 2) iDir = Rnd.Next(4);
                }

                //-------------------------------------------------------------------------------------//
                #endregion

                #region Pr�fenWasAufDemN�chstenFeldIstDannBesetzen

                if (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Empty)
                {
                    CField.oTypeToDraw[iX, iY] = enFieldType.Empty;
                    if (iState == 2) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost;
                    if (iState == 1) CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Pacman;
                    break;
                }

                if (CField.oTypeToDraw[iXNew, iYNew] == enFieldType.Point)
                {
                    if (iState == 1)
                    {
                        CField.oTypeToDraw[iX, iY] = enFieldType.Empty;
                        CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Pacman;
                        Game.iPointS++;
                        break;
                    }

                    if (iState == 2)
                    {
                        CField.oTypeToDraw[iXNew, iYNew] = enFieldType.Ghost;
                        CField.oTypeToDraw[iX, iY] = enFieldType.Point;
                        break;
                    }

                }
                else
                {
                    if (iState == 1) iXNew = iX; iYNew = iY;
                    if (iState == 2) iDir = Rnd.Next(4);
                }

                #endregion


            }
            */
            #endregion

            #region �bertragenVonXundY
            if (iState == 1)
            {
                iPacmanX = iXNew;
                iPacmanY = iYNew;
            }
            if (iState == 2)
            {
                iGhostX1 = iXNew;
                iGhostY1 = iYNew;
            }
            if (iState == 3)
            {
                iGhostX2 = iXNew;
                iGhostY2 = iYNew;
            }
            if (iState == 4)
            {
                iGhostX3 = iXNew;
                iGhostY3 = iYNew;
            }
            if (iState == 5)
            {
                iGhostX4 = iXNew;
                iGhostY4 = iYNew;
            }
            #endregion
        }
        void NewGame()
        {
            iPoints = 0;
            Initialize();
        }
        void StopGame()
        {
            bpaused = true;
        }
        void Init()
        {
            CField = new ClassField();
            CConstants = new ClassConstants();

            iPoints = 0;

            bpaused = false;
            bwon = false;
            blose = false;
            
            iPacmanX = (int)CField.oPacManSpawnPoint.X;
            iPacmanY = (int)CField.oPacManSpawnPoint.Y;

            iGhostX1= (int) CField.oGhostSpawnPoint.X;
            iGhostY1= (int) CField.oGhostSpawnPoint.Y;

            iGhostX2= (int) CField.oGhost2SpawnPoint.X;
            iGhostY2= (int) CField.oGhost2SpawnPoint.Y;

            iGhostX3= (int) CField.oGhost3SpawnPoint.X;
            iGhostY3= (int) CField.oGhost3SpawnPoint.Y;

            iGhostX4= (int) CField.oGhost4SpawnPoint.X;
            iGhostY4 = (int)CField.oGhost4SpawnPoint.Y;
        }
    }
}
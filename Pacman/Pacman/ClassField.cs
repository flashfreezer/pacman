﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;

namespace Pacman
{
    enum enFieldType
    {
        Empty,
        Point,
        Body,
        Pacman,
        Ghost1,
        Ghost2,
        Ghost3,
        Ghost4,
    }

    class ClassField
    {
        public const int iRowCount = 24;
        public const int iColumnCount = 32;
        public int iPointNumber;
        bool bPacManFound = false;
        bool bGhostfound = false;
        int iCurrentRow = 0;
        int iCurrentColumn = 0;

        public enFieldType[,] oTypeToDraw { get; private set; }

        public Vector2 oPacManSpawnPoint { get; private set; }
        
        public Vector2 oGhostSpawnPoint { get; private set; }
        public Vector2 oGhost2SpawnPoint { get; private set; }
        public Vector2 oGhost3SpawnPoint { get; private set; }
        public Vector2 oGhost4SpawnPoint { get; private set; }

        Game oGame;

        /// <summary>
        /// The LevelBuilder reads a Textfile and Builds the list of things to draw for the later drawing of the game.
        /// The list is then aviable in "oTypeToDraw".
        /// 
        /// Note:
        /// . = Point
        /// # = Marks a Place where to Place a solid Block.
        /// 1 = Marks the Spawnpoint of the Pacman.
        /// 2 = Marks the Spawnpoint, where more or less ghosts Spawn.
        /// </summary>

        public ClassField()
        {
            oGame = new Game();
            if (oTypeToDraw == null) LoadFile(@"Levels\level" + oGame.iActualLevel + ".txt");
            else
                return;
        }

        void LoadFile(string sFileName)
        {
            string[] sInputLevel = File.ReadAllLines(sFileName);
            if (sInputLevel.Length != iRowCount)
                throw new InvalidDataException("Falsche Anzahl Zeilen");

            oTypeToDraw = new enFieldType[iColumnCount, iRowCount];
            foreach (string sLineOfString in sInputLevel)
            {
                for (iCurrentColumn = 0; iCurrentColumn < iColumnCount; iCurrentColumn++)
                {
                    if (sLineOfString.Length != iColumnCount)
                        throw new InvalidDataException("Falsche Anzahl Spalten in Reihe" + iCurrentRow);

                    switch (sLineOfString[iCurrentColumn])
                    {
                        case '.': oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Point; iPointNumber++; break;
                        case '#': oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Body; break;
                        case '+': oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Empty; break;
                             
                        case '1':
                            if (bPacManFound)
                                throw new InvalidDataException("There is more than one Spawnpoint for the Pacman");

                            oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Pacman;
                            oPacManSpawnPoint = new Vector2(iCurrentColumn, iCurrentRow);
                            bPacManFound = true;
                            break;

                        case '2':
                            if (bGhostfound)
                                throw new InvalidDataException("There is more than one Spawnpoint for the Ghost");

                            oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Ghost1;
                            oGhostSpawnPoint = new Vector2(iCurrentColumn, iCurrentRow);
                            bGhostfound = true;
                            break;

                       case '3':

                            oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Ghost2;
                            oGhost2SpawnPoint = new Vector2(iCurrentColumn, iCurrentRow);
                            break;
                       
                        case '4':

                            oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Ghost3;
                            oGhost3SpawnPoint = new Vector2(iCurrentColumn, iCurrentRow);
                            break;

                        case '5':

                            oTypeToDraw[iCurrentColumn, iCurrentRow] = enFieldType.Ghost4;
                            oGhost4SpawnPoint = new Vector2(iCurrentColumn, iCurrentRow);
                            break;
                    }
                }
                iCurrentRow++;
            }
        }
    }
}